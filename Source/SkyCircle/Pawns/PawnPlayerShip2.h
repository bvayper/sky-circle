#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"
#include "PawnPlayerShip2.generated.h"

UCLASS()
class SKYCIRCLE_API APawnPlayerShip2 : public APawnBase
{
	GENERATED_BODY()

private:

public:

	virtual void Tick(float DeltaTime) override;

protected:

	virtual void BeginPlay() override;

};