#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"
#include "PawnPlayerShip.generated.h"

UCLASS()
class SKYCIRCLE_API APawnPlayerShip : public APawnBase
{
	GENERATED_BODY()

private:

public:

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

protected:


};