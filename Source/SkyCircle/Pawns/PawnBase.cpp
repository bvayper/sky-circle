#include "PawnBase.h"
#include "Components/CapsuleComponent.h"
#include "SkyCircle/Actors/ProjectileBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APawnBase::APawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
	RootComponent = CapsuleComp;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	BaseMesh->SetupAttachment(RootComponent);

	Bomb1Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bomb1 Mesh"));
	Bomb1Mesh->SetupAttachment(RootComponent);

	Bomb2Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bomb2 Mesh"));
	Bomb2Mesh->SetupAttachment(RootComponent);

	BombSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	BombSpawnPoint->SetupAttachment(BaseMesh);
}

// Called when the game starts or when spawned
void APawnBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APawnBase::Fire()
{
	if (ProjectileClass)
	{
		FVector SpawnLocation = BombSpawnPoint->GetComponentLocation();
		FRotator SpawnRotation = BombSpawnPoint->GetComponentRotation();

		AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(ProjectileClass, SpawnLocation, SpawnRotation);
		TempProjectile->SetOwner(this);

		UE_LOG(LogTemp, Warning, TEXT("%s Throw da bomb"), *GetOwner()->GetName());
	}
}