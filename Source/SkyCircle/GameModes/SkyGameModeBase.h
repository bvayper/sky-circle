#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkyGameModeBase.generated.h"

UCLASS()
class SKYCIRCLE_API ASkyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

private:

public:

protected:
	virtual void BeginPlay() override;
};
